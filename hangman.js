//========================================================================
//   "WebHangman"  JavaScript Hangman  
//
//   Copyright (C) 2000,2002  Jan Mulder
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version, and as long as this notice is
//   kept unmodified at the top of the script source code.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License (license.txt) for more details.
//
//   Contact: Jan Mulder info@EnglishCafe.co.uk
//
//   Last updated: 19th March 2002
//
//=======================================================================
//
// This program has been modified to support Esperanto.
// Chi tiu programo estis modifita por subteni Esperanton.
// Klivo 2003/06
//
// 2003/10/20 Korekto: Uzu unikodan literon en wrongLetters.
// Dankon al Paul Ebermann kiu rimarkis la eraron.
//
var numRight = 0;
var numWrong = 0;
var wordCount = 0;
var doneAction = '';

var thisAnswer;
var thisHint;
var lastAnswer = '';
var answerIdx;
var blankChar = '*';
var answerDisplay;
var maxAttempts = 7;
var usedLetters = '';
var wrongLetters = '';
var maxLength = 0;

var gameOver = false;
var gameStart = true;
var isHint = false;
var imgWidth;
var imgHeight;
var cellDimension;
var firstRun = true;

tempArray = new Array();
wordArray = new Array();

//alphaLower = 'abcdefghijklmnopqrstuvwxyz';

function alfabeto (indekso) {
   var alfa = 'abccdefgghhijjklmnoprsstuuvz';
   if (indekso == 3) return 'cx';
   if (indekso == 8) return 'gx';
   if (indekso == 10) return 'hx';
   if (indekso == 13) return 'jx';
   if (indekso == 22) return 'sx';
   if (indekso == 25) return 'ux';
   return alfa.charAt(indekso);
}

doneLoading = false;
imageCount = 0;
var progressBar = '||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||';

function updateProgress(ims)
{
 var cnt=0;

 for(var i = 0; i < ims.length; i++)
  if(ims[i].complete || ims[i].errored) cnt++;

 if(ims.length > 0)
    window.status='Loading ['+Math.round((cnt / imageCount)*100)+'%] ' + progressBar.substring(0, cnt);

 if(cnt < ims.length)
 {
  tempArray = ims;
  setTimeout("updateProgress(tempArray)",200);
 }
 else
  onComplete();
}

function onComplete()
{
 window.status='Done';
 doneLoading = true;
}

function preloadImages()
{
 this.length = preloadImages.arguments.length;
 imageCount = this.length;
 for (var i = 0; i < this.length; i++)
 {
  this[i] = new Image();
  this[i].errored=false;
  this[i].onerror=new Function("this["+i+"].errored=true");
  this[i].src = preloadImages.arguments[i];
 }
 updateProgress(this);
}

var pictures = new preloadImages(
 sourceDir+"a.gif", sourceDir+"b.gif", sourceDir+"c.gif", sourceDir+"d.gif",
 sourceDir+"e.gif", sourceDir+"f.gif", sourceDir+"g.gif", sourceDir+"h.gif",
 sourceDir+"i.gif", sourceDir+"j.gif", sourceDir+"k.gif", sourceDir+"l.gif",
 sourceDir+"m.gif", sourceDir+"n.gif", sourceDir+"o.gif", sourceDir+"p.gif",
 sourceDir+"q.gif", sourceDir+"r.gif", sourceDir+"s.gif", sourceDir+"t.gif",
 sourceDir+"u.gif", sourceDir+"v.gif", sourceDir+"w.gif", sourceDir+"x.gif",
 sourceDir+"y.gif", sourceDir+"z.gif",
 sourceDir+"pend1.gif", sourceDir+"pend2.gif", sourceDir+"pend3.gif", sourceDir+"pend4.gif",
 sourceDir+"pend5.gif", sourceDir+"pend6.gif", sourceDir+"malvenk1.gif", sourceDir+"malvenk2.gif",
sourceDir+"venk.gif"
);

function doFunction(aFunction)
{
 if (aFunction.indexOf('(') > -1)
   eval( aFunction );
 else
   eval(aFunction+'()');
}

function endGame()
{
  gameOver = true;
  document.control.you.value = numRight;
  document.control.me.value = numWrong;
  if (doneAction != '')
    doFunction(doneAction);
}


// Montru helpfrazon. Tio kostas unu provon.
function showHint()
{

  if (wrongLetters.indexOf(blankChar) == -1)
    wrongLetters = wrongLetters+blankChar;

   if (!gameOver && wrongLetters.length < maxAttempts )
     document.hangimage.src=sourceDir+"pend"+wrongLetters.length+".gif";

  document.hintForm.hint.value=thisHint;
}

function setBrowserInfo(cd, iw, ih)
{
  cellDimension = cd;
  imgWidth = iw;
  imgHeight = ih;
}

function getBrowserInfo()
{
  isOpera = (navigator.userAgent.indexOf('Opera') != -1);

  if (isOpera)
    setBrowserInfo(0, 55, 31);
  else if (navigator.appName == 'Netscape')
    setBrowserInfo(0, 54, 31);
  else
    setBrowserInfo(1, 53, 29);
}

function updateAnswer( stri )
{
  var str = unikodigu(stri);
  for (var i=0; i < str.length; i++)
  {
    if ((lastAnswer.charAt(i) == str.charAt(i)) && (!gameStart))
      continue;
    if (str.charAt(i) == " ")
      eval('document.ans'+i+'.src="'+sourceDir+'space.gif"');
    else if (str.charAt(i) == blankChar)
      eval('document.ans'+i+'.src="'+sourceDir+'blank.gif"');
    else
      eval('document.ans' + i + '.src="' + sourceDir + xigu(str.charAt(i)) + '.gif"');
  }
  lastAnswer = str;

  if (gameStart)
    for (var i=str.length; i < maxLength; i++)
       eval('document.ans'+i+'.src="'+sourceDir+'space.gif"');

  gameStart = false;
}

function arrayDelete( arrayName, delIndex )
{
  var ar = new Array();
  for (var ii = 0; ii < arrayName.length; ii++)
  {
     if (ii != delIndex)
      ar[ar.length] = arrayName[ii];
  }
  return ar;
}

function Word(aWord,aHint)
{
  this.value=aWord;
  this.hint=aHint;
  if (aWord.length > maxLength)
    maxLength = aWord.length;
}

function loadWords( wArray )
{
  var ar = new Array();

  for (var i=0; i < wArray.length; i=i+2)
    ar[ar.length] = new Word(wArray[i], wArray[i+1]);

  wordCount = ar.length;
  return ar;
}

function newPuzzle()
{
  gameOver = false;
  gameStart = true;
  usedLetters = '';
  wrongLetters = '';
  answerDisplay= '';
  lastAnswer = '';

  for (var i=0; i < 26; i++)
    eval('document.img'+alfabeto(i)+'.src="'+sourceDir+alfabeto(i)+'.gif"');

  answerIdx = Math.floor(Math.random()*wordArray.length);
  thisAnswer = unikodigu((wordArray[answerIdx].value).toLowerCase());
  thisHint = unikodigu(wordArray[answerIdx].hint);

  if (wordArray.length > 1)
    wordArray = arrayDelete( wordArray, answerIdx );
  else
    wordArray = loadWords(words);

  for(var i=0; i<thisAnswer.length; i++)
  {
     if ( thisAnswer.charAt(i) == " " )
       answerDisplay = answerDisplay+" ";
     else
       answerDisplay = answerDisplay+blankChar;
  }

  updateAnswer(answerDisplay);

  if (isHint)
    document.hintForm.hint.value=' ';

  document.hangimage.src=sourceDir + "pend0.gif";

  if ((firstRun) && (navigator.appName == 'Netscape'))
    setTimeout('netscapeLoad()',1000);

  firstRun = false;
}

function netscapeLoad()
{
  for (var i=0; i < answerDisplay.length; i++)
    if (answerDisplay.charAt(i) == blankChar)
      eval('document.ans'+i+'.src="'+sourceDir+'blank.gif"');
}

function check(character)
{
  if (gameOver) return;
  if (usedLetters.indexOf(character + '.') == -1)
  {
    usedLetters = usedLetters+character+'.';
    eval('document.img'+character+'.src="'+sourceDir+'space.gif"');
  }
  else
    return;

  var wrongLetter = true;
  for( i = 0; i < thisAnswer.length; i++ )
  {
    if ( thisAnswer.indexOf(unikodigu(character), i) == -1 ) break;

    if ( thisAnswer.charAt(i) == unikodigu(character) )
    {
       wrongLetter = false;
       if( i == 0 )
         answerDisplay = unikodigu(character) + answerDisplay.substring(i+1,thisAnswer.length);
       else if ( i == thisAnswer.length-1 )
         answerDisplay = answerDisplay.substring(0,i) + unikodigu(character);
       else
         answerDisplay = answerDisplay.substring(0,i) + unikodigu(character) +
                                 answerDisplay.substring(i+1,thisAnswer.length);
     }
  }

  if (wrongLetter)
  {
    if (wrongLetters.indexOf(unikodigu(character)) == -1)
      wrongLetters = wrongLetters+unikodigu(character);

     if ( wrongLetters.length < maxAttempts )
       document.hangimage.src=sourceDir+"pend"+wrongLetters.length+".gif";
     else
     {
        updateAnswer(thisAnswer);
        document.hangimage.src = sourceDir+"malvenk1.gif";
        setTimeout('document.hangimage.src = sourceDir+"malvenk2.gif";',1000);
        numWrong++;
        endGame();
     }
  }
  else
  {
    updateAnswer(answerDisplay);

    if( answerDisplay.indexOf(blankChar) == -1 )
    {
      document.hangimage.src=sourceDir+"venk.gif";
      numRight++;
      endGame();
    }
  }
}

function drawAnswer()
{
  wordArray = loadWords(words);
  for (var i=0; i < maxLength; i++)
  {
    document.writeln(
    '<img border=0 SRC=\''
    + sourceDir + 'space.gif\' name="ans'
    + i
    +'">'
    );
  }
  newPuzzle();
}

function drawControl()
{
document.writeln('<FORM name="control">');

document.writeln(
  '<table border="1" bgcolor="#CCCCCC"><tr>'
  + '<TD><input type="button" value="Nova Ludo" onClick = "javascript:void(newPuzzle())">'
  + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vi venkis: <input type="text" size="5" name="you" value="0">'
  + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mi venkis: <input type="text" size="5" name="me" value="0"></TD>'
  + '</tr><tr><TD colspan=3 bgcolor="#FFFFFF" align="center">'
);

document.writeln('<table border="0" bgcolor="#FFFFFF" cellpadding="3"><tr>');
for (var i=0; i < 14; i++)
{
  document.writeln('<td align="center">');

  if (navigator.appName == 'Netscape')
    document.writeln(
    '<A href=\'javascript:void(check("'
    + alfabeto(i)
    + '"))\'>'
    );
  document.writeln(
  '<img width=21 height=21 border=0 SRC=\''
  + sourceDir + alfabeto(i)
  + '.gif\' onClick=\'check("'
  + alfabeto(i)
  + '")\' name="img'
  + alfabeto(i)
  + '"></A></td>'
  );
}

document.writeln('</tr><tr>');

for (var i=14; i < 28; i++)
{
  document.writeln('<td align="center">');

  if (navigator.appName == 'Netscape')
    document.writeln(
    '<A href=\'javascript:void(check("'
    + alfabeto(i)
    + '"))\'>'
    );
  document.writeln(
  '<img border=0 width=21 height=21 SRC=\''
  + sourceDir + alfabeto(i)
  + '.gif\' onClick=\'check("'
  + alfabeto(i)
  + '")\' name="img'
  + alfabeto(i)
  + '"></A></td>'
  );
}

document.writeln('</tr></table></td></tr></table>');
document.writeln('</FORM>');
}

function drawHangman()
{
  document.writeln('<img name="hangimage" src="'+sourceDir+'pend0.gif" width=95 height=137>');
}

function drawHint()
{
  isHint = true;

  document.writeln('<FORM name="hintForm">');
  document.writeln(
  '<table border="1" bgcolor="#CCCCCC"><tr>'
  +'<TD><input type="button" value="Helpo" onClick = "javascript:void(showHint())"></TD>'
  +'<TD><input type="text" size="32" name="hint" value=""></TD>'
  +'</tr></table></FORM>'
  );

}


// Convert Esperanto words from Unicode or circumflex format to x format.
// Konvert Esperantajn vortojn de Unikodo au cirkumfleks-formato al x-formato.
function xigu(originalo) {
   longeco = originalo.length;
   ori = originalo.toLowerCase();
   nova = "";
   nova2 = "";
   litero = '';
   sekvanta = '';
   var m = 0;
   // konvertu ^c
   for (; m < longeco - 1; m++) {
      litero = ori.charAt(m);
      sekvanta = ori.charAt(m+1);
      if (litero == '^') {
         nova += sekvanta + 'x';
         m++;
      }
      else {
         nova += litero;
      }
   }
   if (m < longeco) {
         nova += ori.charAt(m);
   }
   // konvertu unikodon
   m = 0;
   for (; m < longeco; m++) {
      litero = nova.charAt(m);
      if (litero == '\u0109') {
         nova2 += 'cx';
      }
      else if (litero == '\u011d') {
         nova2 += 'gx';
      }
      else if (litero == '\u0125') {
         nova2 += 'hx';
      }
      else if (litero == '\u0135') {
         nova2 += 'jx';
      }
      else if (litero == '\u015d') {
         nova2 += 'sx';
      }
      else if (litero == '\u016d') {
         nova2 += 'ux';
      }
      else nova2 += litero;
   }
   return nova2;
}

function estas_cghjsu(la_litero) {
   if (la_litero == 'c') return true;
   if (la_litero == 'g') return true;
   if (la_litero == 'h') return true;
   if (la_litero == 'j') return true;
   if (la_litero == 's') return true;
   if (la_litero == 'u') return true;
   return false;
}



// Konvertu ^c kaj cx al unikodo.

function unikodigu(originalo) {
   if (originalo == null || originalo == '') return '';
   var longeco = originalo.length;
   var nova = "";
   var litero = '';
   var sekvanta = '';
   var uni_litero = '';
   var i = 0;
   for (; i < longeco - 1; i++) {
      litero = originalo.charAt(i);
      sekvanta = originalo.charAt(i+1);
      if (litero == '^') {
         uni_litero = aski_al_Unikodo(sekvanta);
         if (uni_litero != '*') {
            nova += uni_litero;
            i++;
         }
         else nova += litero;
      }
      else if (sekvanta == 'x' || sekvanta == 'X') {
         uni_litero = aski_al_Unikodo(litero);
         if (uni_litero != '*') {
            nova += uni_litero;
            i++;
         }
         else nova += litero;
      }
      else {
         nova += litero;
      }
   }
   if (i < longeco) {
         nova += originalo.charAt(i);
   }
   return nova;
}

function aski_al_Unikodo (litero) {
   if (litero <= 'z' && litero >= 'A') {
      if (litero == 'c') {
         return '\u0109'; 
      } else 
      if (litero == 'g') {
         return '\u011d'; 
      } else 
      if (litero == 'h') {
         return '\u0125'; 
      } else 
      if (litero == 'j') {
         return '\u0135'; 
      } else 
      if (litero == 's') {
         return '\u015d'; 
      } else 
      if (litero == 'u') {
         return '\u016d'; 
      } else 
      if (litero == 'C') {
         return '\u0108'; 
      } else 
      if (litero == 'G') {
         return '\u011c'; 
      } else 
      if (litero == 'H') {
         return '\u0124'; 
      } else 
      if (litero == 'J') {
         return '\u0134'; 
      } else 
      if (litero == 'S') {
         return '\u015c'; 
      } else 
      if (litero == 'U') {
         return '\u016c'; 
      } else {
         return '*';
      }
   }   // fino de if

   return '*';

}   // fino de aski_al_Unikodo





getBrowserInfo();